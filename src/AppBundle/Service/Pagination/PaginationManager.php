<?php

namespace AppBundle\Service\Pagination;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Common\Collections\ArrayCollection;

class PaginationManager
{
    public static function getPagination(QueryBuilder $qb, $page, $limit)
    {
        $total = clone $qb;
        $total = $total->select(sprintf('COUNT(%s.id)', $total->getAllAliases()[0]))->getQuery()->getSingleScalarResult();

        $pages = ceil($total / $limit);
        
        $page = (int) $page;
        $page = $page <= 1 ? 1 : $page;
        $page = $page > $pages ? $pages : $page;
        
        $paginator = new Paginator($qb, $page);
        
        if ($page > 0) {
            $paginator->getQuery()->setFirstResult($limit * ($page - 1));
            $paginator->getQuery()->setMaxResults($limit);
        }

        return new Pagination($total, $paginator->getQuery()->getResult(), $pages);
    }
}
