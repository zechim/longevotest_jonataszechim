<?php

namespace Zechiani\AppBundle\Twig;

use Symfony\Component\Routing\Router;
use Zechiani\AppBundle\Service\Menu\MenuBuilder;
use Zechiani\AppBundle\Service\Menu\MenuCriteria;
use Zechiani\AppBundle\Service\Menu\MenuItemIterator;
use Zechiani\AppBundle\Entity\Menu;

class MenuExtension extends \Twig_Extension
{   
    /**
     * @var \Twig_Environment
     */
    protected $environment;    
    
    /**
     * @var \Zechiani\AppBundle\Service\Menu\MenuBuilder
     */
    protected $menuBuilder;
    
    /**
     * @var \Symfony\Component\Routing\Router
     */
    protected $router;
    
    public function __construct(Router $router)
    {
        $this->router = $router;
    }
    
    public function getFunctions()
    { 
        return array(
            new \Twig_SimpleFunction('PAGINATION_MANAGER_paginate', [$this, 'paginate'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('MENU_render_submenu', [$this, 'renderSubmenu'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('MENU_generate_route', [$this, 'generateRoute']),
        );
    }
    
    public function renderAll(\Twig_Environment $twig, $pagination)
    {
        return $twig->render('AppBundle::pagination.html.twig', array(
            'pagination' => $pagination,
        ));
    }
    
    public function renderSubmenu(\Twig_Environment $twig, MenuItemIterator $menu, $opened = false)
    {
        return $twig->render('ZechianiAppBundle::Default\menu.submenu.html.twig', [
            'menuList' => $menu,
            'opened' => $opened,
        ]);
    }
    
    public function generateRoute(Menu $menu)
    {
        if ('' === $route = trim($menu->getRoute())) {
            return '#';
        }
        
        $parameters = [];
        
        if ('' !== trim($menu->getParameters())) {
            parse_str($menu->getParameters(), $parameters);   
        }

        return $this->router->generate($route, $parameters);     
    }
        
	/* (non-PHPdoc)
     * @see Twig_ExtensionInterface::getName()
     */
    public function getName()
    {
        return 'zechiani_app_menu_extension';   
    }

}