<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Constraints\NotBlank;
use AppBundle\Entity\Customer;

class TicketType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('customerName', null, [
            'attr' => ['class' => 'form-control'],
            'label' => 'Nome do Cliente',
            'constraints' => [new NotBlank()]
        ]);
        
        $builder->add('saleOrderNumber', null, [
            'attr' => ['class' => 'form-control'],
            'label' => 'Número do Pedido',
            'mapped' => false,
            'constraints' => [new NotBlank()]
        ]);
        
        $builder->add('customerEmail', null, [
            'attr' => ['class' => 'form-control'],
            'label' => 'E-mail',
            'constraints' => [new NotBlank()]
        ]);
        
        $builder->add('title', null, [
            'attr' => ['class' => 'form-control'],
            'label' => 'Título',
            'constraints' => [new NotBlank()]
        ]);
        
        $builder->add('observation', '\Symfony\Component\Form\Extension\Core\Type\TextareaType', [
            'attr' => ['class' => 'form-control'],
            'label' => 'Observação',
            'constraints' => [new NotBlank()]
        ]);
        
        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) use ($options) {
            $customerName = $event->getForm()->get('customerName')->getData();
            $customerEmail = $event->getForm()->get('customerEmail')->getData();
            
            /* @var $em \Doctrine\ORM\EntityManager */
            $em = $options['em'];
            
            $saleOrder = $em->getRepository('AppBundle:SaleOrder')->findOneBy(['number' => $event->getForm()->get('saleOrderNumber')->getData()]);
            
            if (null === $saleOrder) {
                $event->getForm()->get('saleOrderNumber')->addError(new FormError('Pedido não encontrado'));
                
                return;
            }
            
            $customer = $em->getRepository('AppBundle:Customer')->findOneBy(['email' => $customerEmail]);

            if (null === $customer) {
                $customer = new Customer();
                $customer->setEmail($customerEmail);
                $customer->setName($customerName);
                
                $em->persist($customer);
            }

            $event->getData()->setSaleOrder($saleOrder);
        });
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Ticket',
            'em' => null,
        ));
        
        $resolver->setAllowedTypes('em', ['\Doctrine\ORM\EntityManager']);
    }
}
