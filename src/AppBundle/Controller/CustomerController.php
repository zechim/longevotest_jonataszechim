<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Service\Pagination\Pagination;

/**
 * Customer controller.
 *
 * @Route("/customer")
 */
class CustomerController extends Controller
{
    /**
     * Lists all Customer entities.
     *
     * @Route("/{page}", requirements={"page" = "\d+"}, defaults={"page" = 1}, name="customer_index")
     * @Method("GET")
     */
    public function indexAction($page = 1)
    {
        $em = $this->getDoctrine()->getManager();

        $pagination = $em->getRepository('AppBundle:Customer')->getPagination($page);
        $pagination->set(Pagination::ROUTE, 'customer_index');
        
        return $this->render('@AppBundle/Resources/views/customer/index.html.twig', [
            'pagination' => $pagination
        ]);
    }
}
