<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Ticket;    
use AppBundle\Service\Pagination\Pagination;

/**
 * Ticket controller.
 *
 * @Route("/ticket")
 */
class TicketController extends Controller
{
    /**
     * Lists all Ticket entities.
     *
     * @Route("/{page}", requirements={"page" = "\d+"}, defaults={"page" = 1}, name="ticket_index")
     */
    public function indexAction(Request $request, $page = 1)
    {
        $em = $this->getDoctrine()->getManager();

        $criteria = [
            't.customerEmail' => $request->get('email'),
            's.number' => $request->get('number'),
        ];

        $pagination = $em->getRepository('AppBundle:Ticket')->getPagination($page, $criteria);
        $pagination->set(Pagination::ROUTE, 'ticket_index');
        
        $pagination->set(Pagination::PARAMETERS, [
            'email' => $request->get('email'),
            'number' => $request->get('number')
        ]);

        return $this->render('@AppBundle/Resources/views/ticket/index.html.twig', [
            'pagination' => $pagination,
            'flashes' => $this->get('session')->getFlashBag()->get('success', []),
        ] + $pagination->get(Pagination::PARAMETERS));
    }

    /**
     * Creates a new Ticket entity.
     *
     * @Route("/new", name="ticket_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $ticket = new Ticket();
        $form = $this->createForm('AppBundle\Form\TicketType', $ticket, ['em' => $this->getDoctrine()->getManager()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $isValid = $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);
            
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Ticket Criado');
            
            return $this->redirectToRoute('ticket_index');
        }

        return $this->render('@AppBundle/Resources/views/ticket/new.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * Display Ticket
     *
     * @Route("/show/{id}", requirements={"id" = "\d+"}, name="ticket_show")
     * @Method("GET")
     */
    public function showAction(Ticket $ticket)
    {
        return $this->render('@AppBundle/Resources/views/ticket/show.html.twig', [
            'ticket' => $ticket,
        ]);
    }
}
