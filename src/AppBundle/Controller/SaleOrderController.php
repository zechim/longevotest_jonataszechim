<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Service\Pagination\Pagination;

/**
 * SaleOrder controller.
 *
 * @Route("/saleorder")
 */
class SaleOrderController extends Controller
{
    /**
     * Lists all Customer entities.
     *
     * @Route("/{page}", requirements={"page" = "\d+"}, defaults={"page" = 1}, name="saleorder_index")
     * @Method("GET")
     */
    public function indexAction($page = 1)
    {
        $em = $this->getDoctrine()->getManager();
    
        $pagination = $em->getRepository('AppBundle:SaleOrder')->getPagination($page);
        $pagination->set(Pagination::ROUTE, 'saleorder_index');
    
        return $this->render('@AppBundle/Resources/views/saleorder/index.html.twig', [
            'pagination' => $pagination
        ]);
    }
}
