<?php

namespace AppBundle\Entity;

/**
 * Ticket
 */
class Ticket
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $customerName;

    /**
     * @var string
     */
    private $customerEmail;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $observation;

    /**
     * @var \AppBundle\Entity\SaleOrder
     */
    private $saleOrder;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerName
     *
     * @param string $customerName
     *
     * @return Ticket
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;

        return $this;
    }

    /**
     * Get customerName
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * Set customerEmail
     *
     * @param string $customerEmail
     *
     * @return Ticket
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;

        return $this;
    }

    /**
     * Get customerEmail
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Ticket
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set observation
     *
     * @param string $observation
     *
     * @return Ticket
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set saleOrder
     *
     * @param \AppBundle\Entity\SaleOrder $saleOrder
     *
     * @return Ticket
     */
    public function setSaleOrder(\AppBundle\Entity\SaleOrder $saleOrder = null)
    {
        $this->saleOrder = $saleOrder;

        return $this;
    }

    /**
     * Get saleOrder
     *
     * @return \AppBundle\Entity\SaleOrder
     */
    public function getSaleOrder()
    {
        return $this->saleOrder;
    }
}
