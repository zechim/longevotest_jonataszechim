<?php

namespace AppBundle\Entity;

/**
 * Customer
 */
class Customer
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $saleOrders;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->saleOrders = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add saleOrder
     *
     * @param \AppBundle\Entity\SaleOrder $saleOrder
     *
     * @return Customer
     */
    public function addSaleOrder(\AppBundle\Entity\SaleOrder $saleOrder)
    {
        $this->saleOrders[] = $saleOrder;

        return $this;
    }

    /**
     * Remove saleOrder
     *
     * @param \AppBundle\Entity\SaleOrder $saleOrder
     */
    public function removeSaleOrder(\AppBundle\Entity\SaleOrder $saleOrder)
    {
        $this->saleOrders->removeElement($saleOrder);
    }

    /**
     * Get saleOrders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSaleOrders()
    {
        return $this->saleOrders;
    }
}
