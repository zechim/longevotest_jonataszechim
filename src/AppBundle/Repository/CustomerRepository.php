<?php

namespace AppBundle\Repository;

use AppBundle\Service\Pagination\PaginationManager;

/**
 * CustomerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CustomerRepository extends \Doctrine\ORM\EntityRepository
{
    public function getPagination($page, $criteria = [], $limit = 5)
    {
        return PaginationManager::getPagination($this->createQueryBuilder('c'), $page, $limit);
    }
}
